#!/usr/bin/ruby
# -*- coding: utf-8 -*-

require 'getoptlong'

$this = File.basename $0

$version = "1.0.0"
$copyright = "Copyright © Mark Lundquist 2015"
$rights = "All Rights Reserved"
$production = "Produced in the United States of America"
$authors = "Written by Mark Lundquist"

# Thermogenic Effect
# percent
$te_carb = 0.05
$te_fat = 0.00
$te_protein = 0.25

opts = GetoptLong.new(
  [ '--help', '-h', GetoptLong::NO_ARGUMENT ],
  [ '--verbose', '-v', GetoptLong::NO_ARGUMENT ],
  [ '--version', '-V', GetoptLong::NO_ARGUMENT ]
)

def usage()
  puts <<-EOF
Usage: #{$this} [OPTION]... <carb> <fat> <protein> [fiber]

Options:
  -h, --help        show help
  -v, --verbose     verbose output
  -V, --version     show version
EOF
  exit 0
end

def print_version()
  puts <<-EOF
#{$this} #{$version}
#{$copyright}
#{$production}
#{$rights}

#{$authors}
EOF
  exit 0
end

# The degree of ketosis produced by a particular diet has been long determined
# by the Ketogenic Ratio (KR), proposed by R.T. Woodyatt MD, around 1921.
# KR = K / AK
# where, K represents the ketogenic factors and AK represents the anti-ketogenic
# factors.
#
# c = carb (g)
# f = fat (g)
# p = protein (g)
#
#         (0.9 * f) + (0.46 * p)
# KR = ----------------------------
#       c + (0.1 * f) + (0.58 * p)
def calculate_kr(carb, fat, protein)
  ((0.9 * fat) + (0.46 * protein)) / (carb + (0.1 * fat) + (0.58 * protein))
end

def cal_carb(carb_grams)
  carb_grams * 4
end

def cal_fat(fat_grams)
  fat_grams * 9
end

def cal_protein(protein_grams)
  protein_grams * 4
end

# <1 Non-ketogenic
# 1-1.5 Mildly ketogenic
# 1.5-2 Mostly ketogenic
# >2 Very ketogenic
def kr_str(kr)
  case
    when kr < 1.0
      return "Non-ketogenic (<1)"
    when kr > 1.0 && kr <= 1.5
      return "Mildly ketogenic (1-1.5)"
    when kr > 1.5 && kr <= 2.0
      return "Mostly ketogenic (1.5-2)"
    when kr > 2.0
      return "Very ketogenic (>2)"
  end
end

# Handle options
begin
  opts.each do |opt, arg|
    case opt
    when '--help'
      usage
    when '--version'
      print_version
    when '--verbose'
      $verbose = true
    end
  end
rescue
  usage
end

if ARGV[0].nil? || ARGV[0].to_i <= 0
  puts "Must provide carbs (g)"
  usage
end
carb = ARGV[0].to_i

if ARGV[1].nil? || ARGV[1].to_i <= 0
  puts "Must provide fat (g)"
  usage
end
fat = ARGV[1].to_i

if ARGV[2].nil? || ARGV[2].to_i <= 0
  puts "Must provide protein (g)"
  usage
end
protein = ARGV[2].to_i

if ARGV[3].nil? || ARGV[3].to_i <= 0
  fiber = 0
else
  fiber = ARGV[3].to_i
end

# Calculate net carbs
carb = carb - fiber

kr = calculate_kr(carb, fat, protein).round(2)

carb_cal = cal_carb(carb)
fat_cal = cal_fat(fat)
protein_cal = cal_protein(protein)

carb_cal_te = (carb_cal - (carb_cal * $te_carb)).round
fat_cal_te = (fat_cal - (fat_cal * $te_fat)).round
protein_cal_te = (protein_cal - (protein_cal * $te_protein)).round

puts "Ketosis Ratio"
puts "         grams   cals   cals*"
puts "------------------------------"
puts "carb   : #{carb.to_s.rjust(4," ")}   #{carb_cal.to_s.rjust(5," ")}  #{carb_cal_te.to_s.rjust(5," ")}"
puts "fat    : #{fat.to_s.rjust(4," ")}   #{fat_cal.to_s.rjust(5," ")}  #{fat_cal_te.to_s.rjust(5," ")}"
puts "protein: #{protein.to_s.rjust(4," ")}   #{protein_cal.to_s.rjust(5," ")}  #{protein_cal_te.to_s.rjust(5," ")}"
puts "#{(carb_cal + fat_cal + protein_cal).to_s.rjust(21," ")}  #{(carb_cal_te + fat_cal_te + protein_cal_te).to_s.rjust(5," ")}"
puts
puts "KR     : #{kr}"
puts "         #{kr_str(kr)}"
puts
puts "* Adjusted for thermogenic effect of food"
puts
